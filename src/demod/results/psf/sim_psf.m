n=256;

object=zeros(n);
length = 150;
lim=[128-length/2:128+length/2];

object(end-25:end,lim)=1;

subplot(131)
imagesc(object);colormap gray;axis image

kernal_size = 15;

h = fspecial('gaussian',kernal_size,10);
subplot(132)
imagesc(h);axis image

out=conv2(object,h);
out_noiceadd =  out+.3.*rand(size(out));
subplot(133)
imagesc(out_noiceadd);axis image



%% decon
fft_org=fftshift(fft(object));
fft_img=fftshift(fft(out_noiceadd(1:256,1:256)));

figure(2)
psf=fft_org./fft_img;
imagesc(abs(psf))
% imagesc(psf);
