clear all
clc

%sample 2 exp for running on same drive
%file_id = '../Data/1563952038';

%sample running for external Data
fldr = dir('../Data');
fprintf("Select sub folder for analysis\n");
for i = 3:length(fldr)
   fprintf("%d-%s\n",i,fldr(i).name) 
end
fldr_selected = str2num(input('Input folder Id to process > ','s')); 
file_id = strcat(fldr(fldr_selected).folder,'/',fldr(fldr_selected).name)
% try
%     load(strcat(file_id,'/img.mat'))
% end
time_id = 0;    %change to reconstruct for different time step
id = 1;
n = 400;        %lines
dt = 2e-9;      %s
dx = 0.1e-3;    %m
speed=1540;     %ms-1

if exist('img') == 0
    for id = 1:n
        % example file format 10-0.dat <id>-<time_id>.dat
        data = csvread(strcat(file_id,'/',num2str(id),'-',num2str(time_id),'.dat'),7);
        hold on
        if id == 1
            plot(data(:,1))
            hold on
            img=zeros([n,length(data(:,1))]);
        end
        img(id,:)=data(:,1);
        fprintf('%d\n',id)
    end
else
    fprintf('Data exsists\n')
end

save(strcat('../Data/processed/img_full_',fldr(fldr_selected).name,'.mat'),'img');

%%
n = 399;      %lines
dt = 2e-9;    %s
dx = 0.1e-3;  %m
speed=1540; %ms-1
dist_z = speed*dt/2;
down_factor=round(dx/dist_z);

demod_img=img;
for i = 1:size(img,1)
    demod_img(i,:)=demod_try(img(i,:),dt);
end

tmp=downsample(demod_img(1,:),down_factor);
ds_img=zeros([n,length(tmp)]);
for i=1:n+1
    ds_img(i,:)=downsample(demod_img(i,:),down_factor);
end
imagesc(ds_img)