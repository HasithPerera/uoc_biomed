res=.1; %mm
img_length = 60; %mm
seg_length = 20; %mm trnsducer length
ker_len=seg_length/res;
n=img_length/res;

for df_length = [.2 5.2]; %4 mm
    
    
    deltafun = zeros([1 n]);
    deltafun(n/2-(df_length/res)/2:n/2+(df_length/res)/2)=1;
    
    i=0;
    x = 0:ker_len;
    obv_width=[];
    ker_width=[];
    for sz=1:1:ker_len/2-50
        kernal = gaussmf(x,[sz ker_len/2]);
        obv=conv(kernal,deltafun);
        i=i+1;
        obv_width(i)=sum(obv>0.1)*res;
        ker_width(i)=sum(kernal>.1)*res;
    end
    plot(ker_width,obv_width,'o','Linewidth',2)
    hold on
end
xlabel("Kernal Width (mm)")
ylabel("Observation width (mm)")

line([0 25],[15 15])
line([7.1 7.1],[0 30])

% 
% for df_length = [.2 5.2]; %4 mm
%     deltafun = zeros([1 n]);
%     deltafun(n/2-(df_length/res)/2:n/2+(df_length/res)/2)=1;
%     obv_width=[];
%     ker_width=[];
%     i=0;
%     for sz=10:10:ker_len-20
%         kernal = zeros([1 ker_len]);
%         kernal(ker_len/2-sz/2:ker_len/2+sz/2)=1;
%         obv=conv(kernal,deltafun);
%         i=i+1;
%         obv_width(i)=sum(obv>.1)*res;
%         ker_width(i)=sz*res;
%         
%     end  
%     plot(ker_width,obv_width,'o','Linewidth',2)
%     hold on
% end
legend(".2 mm delta","5.2 mm delta")