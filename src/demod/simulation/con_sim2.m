res=.1; %mm
img_length = 60; %mm
seg_length = 20; %mm trnsducer length
ker_len=seg_length/res;
n=img_length/res;
df_length = .2; %mm
lbl = {};
i=0;
for dist=[0,7.3,4,3,2,1,.5,.1]
    i=i+1;
    lbl{i}=num2str(dist)+" mm";
    deltafun = zeros([1 n]);
    loc=n/4;
    deltafun(loc-(df_length/res)/2:loc+(df_length/res)/2)=1;
    % dist=2; %% dist in mm
    loc = loc+ dist/res;
    deltafun(loc-(df_length/res)/2:loc+(df_length/res)/2)=1;
    % plot((1:n)*res,deltafun)
    
    x = 0:ker_len;
    obv_width=[];
    ker_width=[];
    sz=17;
    kernal = gaussmf(x,[sz ker_len/2]);
    obv=conv(kernal,deltafun);
    
    obv_width(i)=sum(obv>0.1)*res;
    ker_width(i)=sum(kernal>.1)*res;
    % subplot(211)
    % plot(deltafun)
    % hold on
    % subplot(212)
    plot((1:800)*res,obv)
    hold on
end

title("Distance")
legend(lbl)
xlabel("location (mm)")