n=100;

sig=zeros([1,n]);
ker=0:1:n-1;
sig(20:40)=1;

sig(60:62)=1;
% sig(78:80)=1;
ker = gaussmf(ker,[5 n/2]);
% plot(sig)
% hold on
% plot(ker)

t=fft(sig);
t1=fft(ker);
ns=(2.*rand(1,n)-1).*.2;
cnv=ifft(t.*t1)+ns;

% plot(cnv)
% recon_ker=ifft(fft(cnv)./t);
% plot(abs(fft(cnv)./t),'o-')
% hold on
% plot(abs(t1),'o-')
% plot(cnv)


cnv2=conv(ker,sig);
ns=(2.*rand(1,length(cnv2))-1).*.5;
cnv2=cnv2+ns;
%built image of the object
plot(cnv2)
hold on

%shifted object function
fk=zeros(size(cnv2));
fk(50:149)=sig;
plot(fk)

sig_deconvolved=deconvwnr(cnv2,ker,1);
plot(sig_deconvolved)
% [q,r]=deconv(cnv2,ker);
% plot(r)

legend('conv','org','deconv')