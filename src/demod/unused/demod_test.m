% %gain 25
% file_id = '../Data/1563948011';
%gain 50
file_id = '../Data/1563948906';


id = 0;
% img = [];
n = 399;      %lines
dt = 2e-9;    %s
dx = 0.1e-3;  %m
speed=1540; %ms-1

if exist('img') == 0
     try
        load(strcat(file_id,'/img_full.mat'))
    end
end

%%
demod_img=img;
for i = 1:size(img,1)
    demod_img(i,:)=demod_try(img(i,:),dt);
end

%% downsample
dist_z = speed*dt/2;
down_factor=round(dx/dist_z);

tmp=downsample(demod_img(1,:),down_factor);
ds_img=zeros([n,length(tmp)]);
for i=1:n+1
    ds_img(i,:)=downsample(demod_img(i,:),down_factor);
end
%%
line=50;
plot(img(line,:))
hold on
plot(abs(hilbert(img(line,:))))
plot(demod_img(line,:))
