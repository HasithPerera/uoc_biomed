clear all
cla()
clc
load('35_fill_lime_no_skin.mat')
i=160;
temp=img(i,:);
% temp=temp+abs(min(temp));
dt=2e-9;
t=0:dt:(length(temp)-1)*dt;
plot(t,temp,'.-')
hold on
f=5e6;
demod=temp.*exp(-j*2*pi*f*t);
plot(t,real(demod))
plot(t,abs(hilbert(temp)))
 xlim([2.8e-5 3.5e-5])
 legend('original','R(S*e^{-j 2\pift})','hilbert transformaion')
 
%  for i=1:size(img,1)
%  demod(i,:)=abs(hilbert(img(i,:)));
%  disp(i)
%  end
 %%
% dt=2e-9;    %s
% dx=0.1e-3; %m
% speed=1496;
% dist_z = speed*(dt/2);
% disp(dx/dist_z)
% 
% tmp=downsample(img(1,:),round(dx/dist_z));
% % ds_img=zeros([n,length(tmp)]);
% for i=1:size(img,1)
%     ds_img(i,:)=downsample(demod(i,:),round(dx/dist_z));
% end
% % %%
% % 
