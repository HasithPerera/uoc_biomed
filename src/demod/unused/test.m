file_id = './1561113112';
id = 1;
% img = [];
n=399;
dt=2e-9;    %s
dx=0.1e-3; %m
speed=1496; %ms-1

for id = 0:n
    data = csvread(strcat(file_id,'/',num2str(id),'.dat'));
%      plot(data(:,1)./1000)
     hold on
    if id == 0
        plot(data(:,1))
        hold on
        [x,y]= ginput(2)
        img=zeros([n,diff(round(x))]);
    end
    %drawnow()
    slctd=[round(x(1)) round(x(2))-1];
    img(id+1,:)=data(slctd(1):slctd(2),1);
    fprintf('%d\n',id)
end


h = figure
imagesc(img)

%%
dist_z = speed*dt;
disp(dx/dist_z)

tmp=downsample(img(1,:),round(dx/dist_z));
ds_img=zeros([n,length(tmp)]);
for i=1:n+1
    ds_img(i,:)=downsample(img(i,:),round(dx/dist_z));
end


imagesc(ds_img')

