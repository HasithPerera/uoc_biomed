% load('../Data/processed/bulk/pla_test1.mat')
disp('load functions')
selected_ids = [1:5]

%state = 0;

%static limits for bulk exp 1
state = 1;
x_start = 23672;
x_len = 9000;
roi_x = (1:x_len)+x_start;
roi_y = 1:400;

index =1;
for i = selected_ids
    
    tmp=data(i).img;
    tmp_dmod = data(i).demod_img';
    if state == 0
        imagesc(tmp)
        [roi_x,roi_y]= sub_roi(tmp,i);
        crop_data = tmp(roi_y,roi_x);
        imagesc(crop_data);
        state = 1;
    end
    figure(1)
    crop_data = tmp(roi_y,roi_x);
    subplot(length(selected_ids),2,index)
    imagesc(crop_data);
    axis off
    subplot(length(selected_ids),2,index+1)
    imagesc(tmp_dmod(roi_y,roi_x));
    axis off
%     imagesc(tmp_dmod(roi_y,roi_x));
    
    
%     figure(2)
%     subplot(length(selected_ids),1,index)
%     imagesc(tmp_dmod(roi_y,roi_x));
    
    index = index +2;
end

function [r_x,r_y] = sub_roi(data,id)
disp("Select a boundry box to enlarge")
[x y]= ginput(2);

x_int = sort(ceil(x));
y_int = sort(ceil(y));

r_x = x_int(1):x_int(2);
r_y = 1:size(data,1);         %x_int(1):x_int(1);
%crop_data = data(roi_y,roi_x);
end