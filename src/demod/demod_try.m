function [mod_sig] = demod_try(raw_sig,dt)
%DEMOD_TRY Summary of this function goes here

df=1/dt;
n_s=length(raw_sig);

f_raw=fft(raw_sig);
freq=df*(0:(n_s/2)-1)/n_s;


%% filter

lim_low= max(find(freq<3.5e6));
lim_high=min(find(freq>8.5e6));

f_mod=f_raw;

f_mod(1:lim_low)=0;
f_mod(n_s-lim_low:end)=0;
f_mod(lim_high:n_s-lim_high)=0;


sig_1=real(ifft(f_mod));
mod_sig = abs(hilbert(sig_1));



end

