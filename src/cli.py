#!/usr/bin/env python3
'''Key press detection to move the printer to the init position '''
import curses
import os
import config as cfg
import datetime
import getpass

import config as cfg
from printercom import *

def old():
    print("Old\r")


def quit():
    print("Quitting\r")
    #break

def up():
    print("Move up\r")
    serial_write("G1 Z{}".format(cfg.step))

def down():
    print("Move Down\r")
    serial_write("G1 Z-{}".format(cfg.step))

def left():
    print("Move Left\r")
    serial_write("G1 X-{}".format(cfg.step))

def right():
    print("Move Right\r")
    serial_write("G1 X{}".format(cfg.step))

def fwd():
    print("Fwd\r")
    serial_write("G1 Y{}".format(cfg.step))

def rev():
    print("Rev\r")
    serial_write("G1 Y-{}".format(cfg.step))

def get_in_num():
    usr_in=input("Enter the step size")
    try:
        cfg.step = int(usr_in)
        print("Step size changed to {}".format(cfg.step))
    except:
        print("Error - Invalid number\r")

def exp_details():

    now = datetime.datetime.now()
    cfg.exp_date = now.strftime("%d-%m-%y %H:%M")
    user = getpass.getuser()
    if (user == 'hasith'):
        cfg.exp_user = 'AHE'
    else:
        cfg.exp_user = input('Please enter your name:')

cmd = {'q':old,
        'KEY_UP':fwd,
        'KEY_DOWN':rev,
        'KEY_LEFT':left,
        'KEY_RIGHT':right,
        'w':up,
        's':down,

        }

def manual_move(win):
    win.nodelay(True)
    key=""
    win.clear()
    print("\nTestti printing\r")
    while 1:
        try:
            key = win.getkey()
            if key == os.linesep:
                break
            else:
                tmp_fun = cmd[str(key)]
                tmp_fun()
        except Exception as e:
           # No input
            pass

def get_exp_text():
    '''Request user to enter experiment details(description,material,gain,author and date)
    '''
    exp_details()
    cfg.exp_text = input("Experiment Description :")
    cfg.exp_material = input("Enter material :")
    in_gain = input('Turn on pulzer\nEnter Gain (dB):')
    cfg.exp_gain = int(in_gain)
