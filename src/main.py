#!/usr/bin/python3
import numpy as np
import matplotlib.pyplot as plt
import serial
import os
import time
import datetime
import getpass

import ctypes
from picosdk.ps3000a import ps3000a as ps
from picosdk.functions import adc2mV, assert_pico_ok

from picofunctions import *
from printercom import *
import config as cfg
from cli import *
from movements import *
from file_io import *

def main():
    print("BioMed-UoC 2019 Ulrasound data acquisition tool\n")


data={}

def move_1():
    print("Move 1")

def old_test():
    main()
    init_mover()
    serial_write("G91") #relative movement

    while(True):
        usr_in = input('>')
        if usr_in == 'q':
            final_move()
            exit(0)
            break
        if usr_in == 'mm':
            set_relative()
            curses.wrapper(manual_move)
        
        if usr_in =='s':
            in2=input("Enter step size:")
            try:
                cfg.step=int(in2)
            except:
                pass

        if usr_in == 'm1':
            line_proc(100)
            print("Reset")
            set_relative()
            serial_write('G1 X-{}'.format(10))
        if usr_in == 'm2':
            line_proc(200)
            print("Reset")
            set_relative()
            print("Reset")
            serial_write('G1 X-{}'.format(20))
        if usr_in == 'm3':
            line_proc(300)
            print("Reset")
            set_relative()
            serial_write('G1 X-{}'.format(30))
        if usr_in == 'm4':
            #step_capture(400,0.1)
            line_proc(400)
            print("Reset")
            set_relative()
            serial_write('G1 X-{}'.format(40))
        if usr_in == 'm10':
            line_proc(700)
            print('Experiment done')
            serial_write('G1 X-{}'.format(70))
        if usr_in == 'sv_ext':
            cfg.data_root = '/media/hasith/AHE-Backup/data/'
            print('data will be saved to {}'.format(cfg.exp_dir))
        if usr_in == 'g':
            grid_proc()
        if usr_in =='tt':
            for jj in range(0,5):
                step_capture(4,10);
                set_relative()
                serial_write('G1 X-{}'.format(40))
                serial_write('G1 Y-{}'.format(10))
            serial_write('G1 Y{}'.format(50))
            print("End Reset")
        if usr_in == 'r':
            print("Reset")
            set_relative()
            serial_write('G1 X{}'.format(cfg.x_lim[0]))
        if usr_in == 'u':
            set_relative()
            serial_write('G1 Z1')
        if usr_in == 'd':
            set_relative()
            serial_write('G1 Z-1')

def get_next_exp_name():
    ls_out = os.popen('ls {}'.format(cfg.data_root)).read()
    exp_files = ls_out.split('\n')
    return int(exp_files[-2])+1

def make_exp_dir():
    try:
        cfg.exp_dir = '{}/{}'.format(cfg.data_root,round(time.time())) 
        os.system('mkdir '+cfg.exp_dir)
    except:
        print("[error] Making current directory ")
    print("[ok] Created {} ".format(cfg.exp_dir))

def write_file(no,dx):
    with open('{}/exp'.format(cfg.exp_dir),'a') as fp:
        fp.write('dx={}\n'.format(dx))
        fp.write('n={}\n'.format(no))
        fp.write('gain={}\n'.format(cfg.exp_gain))
    print('Done writing exp file')
        


def step_capture(no=20,dx=.1):
    set_relative()
    pos_x = 0
    make_exp_dir()
    print(pos_x)
    in_gain=input("Turn on pulzer...\nEnter Gain(dB):")
    cfg.exp_gain=int(in_gain)
    write_file(no,dx)
    for file_no in range(0,no):
        serial_write('G1 X{}'.format(dx))
        #global pos_x
        [a,b,dt,no_of_samples]=get_data(cfg.init_channel_ranges)
        pos_x = pos_x + dx
        
        data = np.array([a,b])
        file_name = '{}/{}.dat'.format(cfg.exp_dir,file_no)
        print('{}-{}'.format(pos_x,file_name))
        try:
            np.savetxt(file_name,np.transpose(data), delimiter=",")
        except:
            print('Error Writing file {}',file_name)
    input("Turn off pulzer")

def grid_proc():
    #make_exp_dir()
    get_exp_text()
    grid(5,5,5,5,t=2)
    print("done experiment")

def line_proc(n):
    ''' line sampelling experiment '''
    get_exp_text()
    line(n)
    #modifd line sample with data header

    print("done experiment")
        
if __name__=='__main__':
    
    connect_printer()
    cfg.init_channel_ranges = setup_init()
    #grid_proc()
    old_test()

    
    #[a,b,t]=get_data(init_channel_ranges)
    #step_capture()
    #test=input('Turn off pulser reciever')
    #final_move()

    #try:
    #except:
    #    print('Picoscope not found')
    #print('[ok]-Picoscope interface') 
    #step_capture()
    #get_next_exp_name()
    #make_exp_dir()
