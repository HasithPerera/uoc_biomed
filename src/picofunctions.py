import ctypes
import numpy as np
from picosdk.ps3000a import ps3000a as ps
from picosdk.functions import adc2mV, assert_pico_ok

import config as cfg

cfg.status = {}
chandle = ctypes.c_int16()

def setup_init():
    '''set inial configuration of th picoscope 3204D'''
    cfg.status["openunit"] = ps.ps3000aOpenUnit(ctypes.byref(chandle), None)
    a_range = 7
    cfg.status["setChA"] = ps.ps3000aSetChannel(chandle, 0, 1, 1, a_range, 0)
    assert_pico_ok(cfg.status["setChA"])
    b_range = 7
    cfg.status["setChB"] = ps.ps3000aSetChannel(chandle, 1, 1, 1, b_range, 0)
    assert_pico_ok(cfg.status["setChB"])
    cfg.status["trigger"] = ps.ps3000aSetSimpleTrigger(chandle, 1, 1, 1024, 2, 0, 1000)
    assert_pico_ok(cfg.status["trigger"])
    Channel_range=[a_range,b_range]
            
    return Channel_range

def get_data(channel_range):
    preTriggerSamples = 5000    #10us
    postTriggerSamples = 70000  #150us
    maxsamples = preTriggerSamples + postTriggerSamples
            
    timebase = 1 #2ns 
    timeIntervalns = ctypes.c_float()
    returnedMaxSamples = ctypes.c_int16()
    cfg.status["GetTimebase"] = ps.ps3000aGetTimebase2(chandle, timebase, maxsamples, ctypes.byref(timeIntervalns), 1, ctypes.byref(returnedMaxSamples), 0)
    assert_pico_ok(cfg.status["GetTimebase"])
    
    # Creates a overlow location for data
    overflow = (ctypes.c_int16)()
    # Creates converted types maxsamples
    cmaxSamples = ctypes.c_int32(maxsamples)

    cfg.status["runblock"] = ps.ps3000aRunBlock(chandle, preTriggerSamples, postTriggerSamples, timebase, 1, None, 0, None, None)
    assert_pico_ok(cfg.status["runblock"])
    
    # Create buffers ready for assigning pointers for data collection
    bufferA = (ctypes.c_int16 * maxsamples)()
    bufferA_down = (ctypes.c_int16 * maxsamples)() # used for downsampling which isn't in the scope of this example
    
    bufferB = (ctypes.c_int16 * maxsamples)()
    bufferB_down = (ctypes.c_int16 * maxsamples)()
    

    cfg.status["SetDataBuffersA"] = ps.ps3000aSetDataBuffers(chandle, 0, ctypes.byref(bufferA), ctypes.byref(bufferA_down), maxsamples, 0, 0)
    assert_pico_ok(cfg.status["SetDataBuffersA"])
    
    
    cfg.status["SetDataBuffersB"] = ps.ps3000aSetDataBuffers(chandle, 1, ctypes.byref(bufferB), ctypes.byref(bufferB_down), maxsamples, 0, 0)
    assert_pico_ok(cfg.status["SetDataBuffersB"])
    
    # Checks data collection to finish the capture
    ready = ctypes.c_int16(0)
    check = ctypes.c_int16(0)
    while ready.value == check.value:
        cfg.status["isReady"] = ps.ps3000aIsReady(chandle, ctypes.byref(ready))
    
    cfg.status["GetValuesA"] = ps.ps3000aGetValues(chandle, 0, ctypes.byref(cmaxSamples), 0, 0, 0, ctypes.byref(overflow))
    assert_pico_ok(cfg.status["GetValuesA"])

    # Finds the max ADC count
    maxADC = ctypes.c_int16()
    cfg.status["maximumValue"] = ps.ps3000aMaximumValue(chandle, ctypes.byref(maxADC))
    assert_pico_ok(cfg.status["maximumValue"])
    channel_a = np.array(adc2mV(bufferA, channel_range[0], maxADC))
    channel_b = np.array(adc2mV(bufferB, channel_range[1], maxADC))
    #print('t_int:{}\nt_max:{}'.format(timeIntervalns.value,cmaxSamples.value))
    #time = np.linspace(0, (cmaxSamples.value) * timeIntervalns.value, cmaxSamples.value)
    return channel_a,channel_b,timeIntervalns.value,cmaxSamples.value

def get_array_data():
    ''' get formatted array for file write'''
    [a,b,dt,no_of_samples]=get_data(cfg.init_channel_ranges)
    data = np.array([a,b])
    return data

