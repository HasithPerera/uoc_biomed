import numpy as np
import time
import config as cfg
import os
'''File operations'''

def sample_save(file_id,t,data):
    '''Read picoscope and save file with header'''
    header = '''exp_id:{}
time_step:{}
exp_date:{}
exp_text:{}
exp_material:{}
exp_gain:{}
exp_user:{}'''.format(file_id,t,cfg.exp_date,cfg.exp_text,cfg.exp_material,cfg.exp_gain,cfg.exp_user)
    file_name = '{}/{}-{}.dat'.format(cfg.exp_dir,file_id,t)
    try:
        np.savetxt(file_name,np.transpose(data), delimiter=",",header=header)
    except:
        print('! Writing file {}'.format(file_name))


def make_exp_dir():
    '''Make a new directory for experiments inside the folder specified by cfg.data_root (eg : ./src/Data)
current ./src/Data is ignored in git
    '''
    try:
        cfg.exp_dir = '{}/{}'.format(cfg.data_root,round(time.time())) 
        os.system('mkdir '+cfg.exp_dir)
    except:
        print("[error] Making current directory ")
    print("[ok] Created {} ".format(cfg.exp_dir))

