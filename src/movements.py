''' This contain functions to achieve different movements '''
import os
from picofunctions import *
from printercom import *
from file_io import *


def get_time_n(file_id,t=1):
    '''Multiple time step save '''
    for t_step in range(0,t):
        #print("{}-{}".format(file_id,t_step))
        sample_save(file_id,t_step,get_array_data())

def grid(no_x,no_y,dx=5,dy=5,t=1,debug=0):
    '''Grid move with defined no of steps Maximum travel distance '''
    if debug == 1:
        print('''Debug mode:''')
    set_relative()
    make_exp_dir()
    direction = 1
    file_id = 1;
    for x in range(0,no_x):
        get_time_n(file_id,t)
        for y in range(0,no_y-1):
            serial_write("G1 Y{}".format(direction*dy))
            file_id = file_id + 1
            get_time_n(file_id,t)
        direction = direction*-1
        if (x==no_x-1):
            pass
        else:
            serial_write("G1 X{}".format(dx))
    serial_write("G1 X{}".format(-1*dx*(no_x-1)))

def line(no_x,dx=.1,t=1,debug=0):
    ''' Single line sample in x dimention '''
    if debug == 1:
        print('Debug mode:')
    set_relative()
    make_exp_dir()
    file_id = 1
    for x in range(0,no_x):
        get_time_n(file_id,t)
        file_id = file_id + 1 
        serial_write("G1 X{}".format(dx))

