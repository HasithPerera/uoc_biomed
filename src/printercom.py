#!/usr/bin/env python3
'''Merlin Gcode interfacing and movement functions'''
try:
    import serial
except:
    print('Py-serial not found')
    pass
import config as cfg

x_lim = (30,100)
y_lim = (70,130)

def connect_printer(port='/dev/ttyUSB0'):
    ''' Try to establish a connection with a specified serial port with required settings for merlin firmware'''
    cfg.ser = serial.Serial()
    cfg.ser.baudrate = 115200
    cfg.ser.port = port
    try:
        cfg.ser.open()
    except:
        print("[{}]\tError opening port")
    
    return cfg.ser

def home_z():
    serial_write('G28 Z')

def home_x_y():
    serial_write('G28 X Y')

def travel_level():
    serial_write('G1 Z85')

def move_to_sample():
    serial_write('G1 Z85')
    serial_write('G1 X65 Y100')
    #serial_write('G1 Z0')
def check_bound():
    '''uses the global parameters x_lim and y_lim to check for movement violataions'''
    serial_write('G1 X{}Y{}'.format(x_lim[0],y_lim[0]))
    serial_write('M400')
    serial_write('G1 X{}Y{}'.format(x_lim[1],y_lim[0]))
    serial_write('M400')
    serial_write('G1 X{}Y{}'.format(x_lim[1],y_lim[1]))
    serial_write('M400')
    serial_write('G1 X{}Y{}'.format(x_lim[0],y_lim[1]))
    serial_write('M400')

def final_move():
    ''' The function befor exiting the connection.
    The endritractor is take to the home position and serial communication is closed'''

    print('Preparing to quit...')
    set_abs()
    travel_level()
    #home_x_y()
    serial_write('G28 X')
    serial_write('M400')
    
    print('Homeing Z....')
    home_z()
    serial_write('G1 Y200')
    print('Turn off power')

def set_abs():
    '''Set absolute positioining '''
    print('Setting ABS mode')
    serial_write('G90')

def set_relative():
    '''Set relative mode '''
    print('Set relative movement')
    serial_write('G91')

def serial_write(command):
    '''Serial write with utf encoding and \\n and  \\r endings'''
    #global ser
    command = command + '\r\n'      
    cfg.ser.reset_input_buffer()
    cfg.ser.write(command.encode('utf-8'))
    reply_to_cmd = cfg.ser.readline()
    #print("rply:"+reply_to_cmd.decode('utf-8'))
    return reply_to_cmd.decode('utf-8')    

def wait_printer():
    '''Handels the iniial serial prints done on the restart waits till it is ready to respond o serial commands '''
    #global ser
    #in_data=ser.readline()
    while True:
        rply=serial_write('G0')
        #print(len(rply))
        if rply=="D\n":
            break

        #print(in_data.decode('utf-8'))
def init_mover(port='/dev/ttyUSB0'):
    ''' Iniatates serial connectio with the given port.'''
    #ser = connect_printer(port)
    wait_printer()
    home_z()
    travel_level()
    home_x_y()
    move_to_sample()
    check_bound()

if __name__=='__main__':
    print('Testing by directly executing the main function')
    cfg.ser = connect_printer('/dev/ttyUSB0')
    init_mover()
    while (True):
        try:
            text=input('>')
            if text == 'h':
                home_z()
                print('home-X Y')
            if text == 'start':
                move_to_sample()
        except:
            print('quit')
            final_move()
            break

