# uoc_biomed

Contains the new python implementation of the ultrasound data acquisition platform  
## Installation

picosdk is used to interface the picoscode 3204D  
follow the instructions found [here](https://github.com/picotech/picosdk-python-wrappers/)

## Added features
- [x] Serial interface
    - [x] Merlin 1.9 firmware modifications to G1 move complete
    - [x] init_comm
    - [x] programmed movements
- [ ] Picoscope interface
    - [x] Block mode
    - [x] Trigerring
    - [ ] Continous mode
- [ ] File operations
    - [x] write data to csv
    - [ ] read experiment sequence from file
    - [ ] Headder file
    - [ ] image saving and creation
    - [ ] write binary file

